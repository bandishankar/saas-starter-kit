# `/tools`

Supporting tools for this project. Note that these tools can import code from the /pkg and /internal directories.

Current Tools:
* [schema](https://gitlab.com/saas/test-project/tree/master/tools/schema) - A command line tool for 
local development that executes database migrations. 

* [text-translator](https://gitlab.com/saas/test-project/tree/master/tools/text-translator) - A 
command line tool for supporting internazionalied go-templates.
